﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void GameMechanicHandler::Start()
extern void GameMechanicHandler_Start_m4F160E5FFED0291AD6B9978E2521343D59E12B2C (void);
// 0x00000002 System.Void GameMechanicHandler::SwitchWorlds()
extern void GameMechanicHandler_SwitchWorlds_mD791048A386C3301597386BE747EA972E8983202 (void);
// 0x00000003 System.Void GameMechanicHandler::CheckCurrentWorld()
extern void GameMechanicHandler_CheckCurrentWorld_m149F9F648A4FEAA608C1204A8130046CC7A0CEA3 (void);
// 0x00000004 System.Void GameMechanicHandler::ReturnToReal()
extern void GameMechanicHandler_ReturnToReal_m4110C6E90EFA258CADC36FFA29D9D883C38293AA (void);
// 0x00000005 System.Void GameMechanicHandler::.ctor()
extern void GameMechanicHandler__ctor_m4A249062503C233B44636F55EB680ABC91C91A25 (void);
// 0x00000006 System.Void PlayerMovement::Start()
extern void PlayerMovement_Start_mB585552228B1908E44D3A69496598FB485F608B6 (void);
// 0x00000007 System.Void PlayerMovement::Update()
extern void PlayerMovement_Update_mC3491BD6CDFF1FA543B16969144C939B2298052F (void);
// 0x00000008 System.Void PlayerMovement::FixedUpdate()
extern void PlayerMovement_FixedUpdate_m774280268A537B6ED9D9171CEAE67E9A0C3A9499 (void);
// 0x00000009 System.Void PlayerMovement::MovementInput()
extern void PlayerMovement_MovementInput_mBE83F644FD3FC1A4C9AB31083B492DB5074ACCAF (void);
// 0x0000000A System.Void PlayerMovement::StopMovement()
extern void PlayerMovement_StopMovement_m99A1423E93F6C710214C1518E1F9F2D9606645A9 (void);
// 0x0000000B System.Void PlayerMovement::.ctor()
extern void PlayerMovement__ctor_mBF9F632DD9929DD6FF092A968649A4406BFE397F (void);
// 0x0000000C System.Void SceneManagerScript::LoadGame()
extern void SceneManagerScript_LoadGame_m19D413A7B54721C7A44C828B6D0A8CDC295BAE45 (void);
// 0x0000000D System.Void SceneManagerScript::GoToStart()
extern void SceneManagerScript_GoToStart_m04004D95488E919F27A79BECDB005244456CA174 (void);
// 0x0000000E System.Void SceneManagerScript::QuitGame()
extern void SceneManagerScript_QuitGame_m44AAFABEFA6F6B27621B5080DEFAE451A237175B (void);
// 0x0000000F System.Collections.IEnumerator SceneManagerScript::WaitForMusic(System.String)
extern void SceneManagerScript_WaitForMusic_m209033F90A4774BD0F4703FDA33C96309931A84E (void);
// 0x00000010 System.Void SceneManagerScript::.ctor()
extern void SceneManagerScript__ctor_mD2F136792DEC86FB19916D427AF0E2FAFFF6FADD (void);
// 0x00000011 System.Void SceneManagerScript/<WaitForMusic>d__3::.ctor(System.Int32)
extern void U3CWaitForMusicU3Ed__3__ctor_m27A6C096A2254C19B11B785B38F269AB2E5A836D (void);
// 0x00000012 System.Void SceneManagerScript/<WaitForMusic>d__3::System.IDisposable.Dispose()
extern void U3CWaitForMusicU3Ed__3_System_IDisposable_Dispose_mDF2B5E8C67FA702BE24C247392F89BA13B0081CB (void);
// 0x00000013 System.Boolean SceneManagerScript/<WaitForMusic>d__3::MoveNext()
extern void U3CWaitForMusicU3Ed__3_MoveNext_m4EFA98C0468D4CE4170183110BB2E04BE0C538C8 (void);
// 0x00000014 System.Object SceneManagerScript/<WaitForMusic>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForMusicU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6247BDEBF518B84C2B2C0A2522661425BF6A5A2B (void);
// 0x00000015 System.Void SceneManagerScript/<WaitForMusic>d__3::System.Collections.IEnumerator.Reset()
extern void U3CWaitForMusicU3Ed__3_System_Collections_IEnumerator_Reset_m1B53FA8429CBC747A1D5CF13483E53E3B53EA925 (void);
// 0x00000016 System.Object SceneManagerScript/<WaitForMusic>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForMusicU3Ed__3_System_Collections_IEnumerator_get_Current_m38349FC2B40D3D594934D791CB51071C26922E94 (void);
// 0x00000017 System.Void StoryManager::Start()
extern void StoryManager_Start_mFFB491D0C43EEF65A039337AFBBD9A114A9A688A (void);
// 0x00000018 System.Void StoryManager::Scenario1()
extern void StoryManager_Scenario1_mCCC398CD46141902B6D7324D2D8D61902D853BF8 (void);
// 0x00000019 System.Void StoryManager::Scenario2()
extern void StoryManager_Scenario2_m50CBA12AABEAB75293A2C1B528A8B77440B6890C (void);
// 0x0000001A System.Void StoryManager::Scenario3()
extern void StoryManager_Scenario3_mD0110D3AF5DE1235EB297177124CB1AC82761AB0 (void);
// 0x0000001B System.Void StoryManager::OpenText()
extern void StoryManager_OpenText_m6563ECF65AA06B725F459C360DDBDE8EAAB697FB (void);
// 0x0000001C System.Void StoryManager::CloseText()
extern void StoryManager_CloseText_mD55A029E2E39963A59FE23FA8D8B1D2377165B47 (void);
// 0x0000001D System.Void StoryManager::OpenButton()
extern void StoryManager_OpenButton_m96FB28C1206E5A86A9660A053DDD14D01D6FF3C2 (void);
// 0x0000001E System.Void StoryManager::CloseButton()
extern void StoryManager_CloseButton_mA6AA8EE53DBF234388EEF2402D986C7ABE90E478 (void);
// 0x0000001F System.Void StoryManager::ChangeScenario()
extern void StoryManager_ChangeScenario_m777146EDCFCB8C231B7A96424DF6791D0FE985AC (void);
// 0x00000020 System.Void StoryManager::EndScene()
extern void StoryManager_EndScene_m12D3E11095640E85BD732F6848B4415E2802100D (void);
// 0x00000021 System.Collections.IEnumerator StoryManager::FadeOut()
extern void StoryManager_FadeOut_m2E33B154C8C26EE59303DF0369ED8C78A13451AB (void);
// 0x00000022 System.Void StoryManager::.ctor()
extern void StoryManager__ctor_mC46D5758437945C4426313A446052B2EDD93F494 (void);
// 0x00000023 System.Void StoryManager/<FadeOut>d__26::.ctor(System.Int32)
extern void U3CFadeOutU3Ed__26__ctor_mEF4E2B9149B34412543D9DB6A9230809FA98EBA1 (void);
// 0x00000024 System.Void StoryManager/<FadeOut>d__26::System.IDisposable.Dispose()
extern void U3CFadeOutU3Ed__26_System_IDisposable_Dispose_m099C4F8009EC75FE1B05D46F73FD69A6823885C9 (void);
// 0x00000025 System.Boolean StoryManager/<FadeOut>d__26::MoveNext()
extern void U3CFadeOutU3Ed__26_MoveNext_m91B1593DF02D0FA212FB4E4C9BA75757A07F89E7 (void);
// 0x00000026 System.Object StoryManager/<FadeOut>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeOutU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m134EF6A6F1B72083A1C97DA659257F96F2DBE632 (void);
// 0x00000027 System.Void StoryManager/<FadeOut>d__26::System.Collections.IEnumerator.Reset()
extern void U3CFadeOutU3Ed__26_System_Collections_IEnumerator_Reset_m426FED76192FA9002D85B817F3F4FBEC276273D4 (void);
// 0x00000028 System.Object StoryManager/<FadeOut>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CFadeOutU3Ed__26_System_Collections_IEnumerator_get_Current_mADC9B3AB5BA1C0B26EDE43A4A80D9B2113D5B6B1 (void);
// 0x00000029 System.Void StoryTriggerManager::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void StoryTriggerManager_OnTriggerEnter2D_mED02C9CAD6709F05CC573A23CDE65A679A2E8048 (void);
// 0x0000002A System.Void StoryTriggerManager::OnTriggerExit2D(UnityEngine.Collider2D)
extern void StoryTriggerManager_OnTriggerExit2D_m8DF2CDD4F3CEA3393A360A242DD0FF94445739A8 (void);
// 0x0000002B System.Void StoryTriggerManager::.ctor()
extern void StoryTriggerManager__ctor_m1365028785E955C413D458687BD2742F751D474E (void);
static Il2CppMethodPointer s_methodPointers[43] = 
{
	GameMechanicHandler_Start_m4F160E5FFED0291AD6B9978E2521343D59E12B2C,
	GameMechanicHandler_SwitchWorlds_mD791048A386C3301597386BE747EA972E8983202,
	GameMechanicHandler_CheckCurrentWorld_m149F9F648A4FEAA608C1204A8130046CC7A0CEA3,
	GameMechanicHandler_ReturnToReal_m4110C6E90EFA258CADC36FFA29D9D883C38293AA,
	GameMechanicHandler__ctor_m4A249062503C233B44636F55EB680ABC91C91A25,
	PlayerMovement_Start_mB585552228B1908E44D3A69496598FB485F608B6,
	PlayerMovement_Update_mC3491BD6CDFF1FA543B16969144C939B2298052F,
	PlayerMovement_FixedUpdate_m774280268A537B6ED9D9171CEAE67E9A0C3A9499,
	PlayerMovement_MovementInput_mBE83F644FD3FC1A4C9AB31083B492DB5074ACCAF,
	PlayerMovement_StopMovement_m99A1423E93F6C710214C1518E1F9F2D9606645A9,
	PlayerMovement__ctor_mBF9F632DD9929DD6FF092A968649A4406BFE397F,
	SceneManagerScript_LoadGame_m19D413A7B54721C7A44C828B6D0A8CDC295BAE45,
	SceneManagerScript_GoToStart_m04004D95488E919F27A79BECDB005244456CA174,
	SceneManagerScript_QuitGame_m44AAFABEFA6F6B27621B5080DEFAE451A237175B,
	SceneManagerScript_WaitForMusic_m209033F90A4774BD0F4703FDA33C96309931A84E,
	SceneManagerScript__ctor_mD2F136792DEC86FB19916D427AF0E2FAFFF6FADD,
	U3CWaitForMusicU3Ed__3__ctor_m27A6C096A2254C19B11B785B38F269AB2E5A836D,
	U3CWaitForMusicU3Ed__3_System_IDisposable_Dispose_mDF2B5E8C67FA702BE24C247392F89BA13B0081CB,
	U3CWaitForMusicU3Ed__3_MoveNext_m4EFA98C0468D4CE4170183110BB2E04BE0C538C8,
	U3CWaitForMusicU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6247BDEBF518B84C2B2C0A2522661425BF6A5A2B,
	U3CWaitForMusicU3Ed__3_System_Collections_IEnumerator_Reset_m1B53FA8429CBC747A1D5CF13483E53E3B53EA925,
	U3CWaitForMusicU3Ed__3_System_Collections_IEnumerator_get_Current_m38349FC2B40D3D594934D791CB51071C26922E94,
	StoryManager_Start_mFFB491D0C43EEF65A039337AFBBD9A114A9A688A,
	StoryManager_Scenario1_mCCC398CD46141902B6D7324D2D8D61902D853BF8,
	StoryManager_Scenario2_m50CBA12AABEAB75293A2C1B528A8B77440B6890C,
	StoryManager_Scenario3_mD0110D3AF5DE1235EB297177124CB1AC82761AB0,
	StoryManager_OpenText_m6563ECF65AA06B725F459C360DDBDE8EAAB697FB,
	StoryManager_CloseText_mD55A029E2E39963A59FE23FA8D8B1D2377165B47,
	StoryManager_OpenButton_m96FB28C1206E5A86A9660A053DDD14D01D6FF3C2,
	StoryManager_CloseButton_mA6AA8EE53DBF234388EEF2402D986C7ABE90E478,
	StoryManager_ChangeScenario_m777146EDCFCB8C231B7A96424DF6791D0FE985AC,
	StoryManager_EndScene_m12D3E11095640E85BD732F6848B4415E2802100D,
	StoryManager_FadeOut_m2E33B154C8C26EE59303DF0369ED8C78A13451AB,
	StoryManager__ctor_mC46D5758437945C4426313A446052B2EDD93F494,
	U3CFadeOutU3Ed__26__ctor_mEF4E2B9149B34412543D9DB6A9230809FA98EBA1,
	U3CFadeOutU3Ed__26_System_IDisposable_Dispose_m099C4F8009EC75FE1B05D46F73FD69A6823885C9,
	U3CFadeOutU3Ed__26_MoveNext_m91B1593DF02D0FA212FB4E4C9BA75757A07F89E7,
	U3CFadeOutU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m134EF6A6F1B72083A1C97DA659257F96F2DBE632,
	U3CFadeOutU3Ed__26_System_Collections_IEnumerator_Reset_m426FED76192FA9002D85B817F3F4FBEC276273D4,
	U3CFadeOutU3Ed__26_System_Collections_IEnumerator_get_Current_mADC9B3AB5BA1C0B26EDE43A4A80D9B2113D5B6B1,
	StoryTriggerManager_OnTriggerEnter2D_mED02C9CAD6709F05CC573A23CDE65A679A2E8048,
	StoryTriggerManager_OnTriggerExit2D_m8DF2CDD4F3CEA3393A360A242DD0FF94445739A8,
	StoryTriggerManager__ctor_m1365028785E955C413D458687BD2742F751D474E,
};
static const int32_t s_InvokerIndices[43] = 
{
	2643,
	2643,
	2643,
	2643,
	2643,
	2643,
	2643,
	2643,
	2643,
	2643,
	2643,
	2643,
	2643,
	2643,
	1680,
	2643,
	2162,
	2643,
	2607,
	2577,
	2643,
	2577,
	2643,
	2643,
	2643,
	2643,
	2643,
	2643,
	2643,
	2643,
	2643,
	2643,
	2577,
	2643,
	2162,
	2643,
	2607,
	2577,
	2643,
	2577,
	2178,
	2178,
	2643,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	43,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
