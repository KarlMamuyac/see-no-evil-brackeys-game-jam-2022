using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagerScript : MonoBehaviour
{
    public void LoadGame(){
        StartCoroutine(WaitForMusic("SampleScene"));
    }

    public void GoToStart(){
        StartCoroutine(WaitForMusic("StartMenu"));
    }

    public void QuitGame(){
        Application.Quit();
    }

    IEnumerator WaitForMusic(string txt){
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(txt);
    }
}
