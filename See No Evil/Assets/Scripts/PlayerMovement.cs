using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpd = 5f;
    private Rigidbody2D playerRb;
    private Vector2 movement;

    private Animator playerAnimator;

    void Start(){
        playerRb = GetComponent<Rigidbody2D>();
        playerAnimator = GetComponentInChildren<Animator>();
    }

    void Update(){
        MovementInput();
    }

    void FixedUpdate(){
        playerRb.MovePosition(playerRb.position + movement * moveSpd *Time.fixedDeltaTime);
    }

    void MovementInput(){
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        playerAnimator.SetFloat("Horizontal", movement.x);
        playerAnimator.SetFloat("Vertical", movement.y);
        playerAnimator.SetFloat("Speed", movement.sqrMagnitude);
    }

    public void StopMovement(){
        playerAnimator.SetFloat("Horizontal", 0);
        playerAnimator.SetFloat("Vertical",0);
        playerAnimator.SetFloat("Speed", 0);
    }

}
