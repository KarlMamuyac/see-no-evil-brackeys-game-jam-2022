using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryTriggerManager : MonoBehaviour
{
    public int scenarioNum;
    public StoryManager sManager;
    public GameObject eTrigger2;

    void OnTriggerEnter2D(Collider2D col)
    {
        switch(scenarioNum){
            case 1:
                sManager.Scenario1();
                sManager.OpenButton();
                sManager.OpenText();
                eTrigger2.SetActive(false);
                break;
            case 2:
                sManager.Scenario2();
                sManager.OpenText();
                eTrigger2.SetActive(true);
                break;
            case 3:
                sManager.Scenario3();
                sManager.CloseButton();
                sManager.OpenText();
                sManager.EndScene();
                break;
            case 4:
                sManager.ChangeScenario();
                this.enabled = false;
                break;
            default:
                break;
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        sManager.CloseText();
    }
}
