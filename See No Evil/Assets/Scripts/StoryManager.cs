using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class StoryManager : MonoBehaviour
{
    string s1 = "Hmm? There used to be a door here.";
    string s2 = "Alright. Time to go back to bed.";
    string s3 = "Ş̷̻̀̕w̵͈̙̙̬͚͠e̷̡̒e̷̝̮̪͕̒̾̋͋t̵̡̙̹̏̕ ̷̬͖͓̾d̵̞̜̅ȑ̸̼̰̪̲̬̄̅́ẻ̶̢à̶͓̝̻̤̿́͝m̷̱͒̃̚s̵̯͚̩̏͐ ̸͎̘͂̎̓ç̵̡͎̼͉̓̒̆̍h̴̛̻̮̑̓í̸̮͒l̴͎̺̲̭͋͋̽d̴̢̮͒̀̾̃ ";

    public TextMeshProUGUI textDisplay;
    public GameObject textPanel;
    public GameObject buttonVL;

    public GameObject trigger1;
    public GameObject trigger2;
    public GameObject trigger3;

    public GameObject endPanel;

    private int scenarioNum = 0;

    public GameObject playerCont;
    private PlayerMovement playerMov;
    public GameMechanicHandler gHandler;

    public AudioClip endRing;
    public AudioSource bgm;
    
    void Start(){
        endPanel.SetActive(false);
        CloseText();
        CloseButton();
        ChangeScenario();
        playerMov = playerCont.GetComponent<PlayerMovement>();
    }

    public void Scenario1(){
        textDisplay.text = s1;
    }

    public void Scenario2(){
        textDisplay.text = s2;
    }

    public void Scenario3(){
        textDisplay.text = s3;
    }

    public void OpenText(){
        textPanel.SetActive(true);
    }

    public void CloseText(){
        textPanel.SetActive(false);
    }

    public void OpenButton(){
        buttonVL.SetActive(true);
    }

    public void CloseButton(){
        buttonVL.SetActive(false);
    }

    public void ChangeScenario(){
        scenarioNum++;
        switch(scenarioNum){
            case 1:
                trigger1.SetActive(true);
                trigger2.SetActive(false);
                trigger3.SetActive(false);
                break;
            case 2:
                trigger1.SetActive(false);
                trigger2.SetActive(true);
                trigger3.SetActive(false);
                break;
            case 3:
                trigger1.SetActive(false);
                trigger2.SetActive(false);
                trigger3.SetActive(true);
                break;
            default:
                break;
        }
    }

    public void EndScene(){
        bgm.Stop();
        bgm.PlayOneShot(endRing, 0.75f);
        playerMov.StopMovement();
        gHandler.ReturnToReal();
        playerMov.enabled = false;
        endPanel.SetActive(true);
        StartCoroutine(FadeOut());
    }

    IEnumerator FadeOut(){
        yield return new WaitForSeconds(10f);
        SceneManager.LoadScene("Credits");
    }
}
