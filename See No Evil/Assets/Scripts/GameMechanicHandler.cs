using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMechanicHandler : MonoBehaviour
{
    [SerializeField]
    private bool isReal = true;
    [SerializeField]
    private GameObject vividWorld;
    [SerializeField]
    private GameObject lucidWorld;
    [SerializeField]
    private GameObject screenVolume;

    [SerializeField]
    private Button buttonVL;
    
    public Sprite openEye;
    public Sprite closedEye;


    void Start() {
        CheckCurrentWorld();
    }

    public void SwitchWorlds(){
        isReal = !isReal;
        CheckCurrentWorld();
    }

    void CheckCurrentWorld(){
        vividWorld.SetActive(isReal);
        lucidWorld.SetActive(!isReal);
        screenVolume.SetActive(!isReal);

        if(!isReal){
            buttonVL.GetComponent<Image>().sprite = openEye;
        }
        if(isReal){
             buttonVL.GetComponent<Image>().sprite = closedEye;
        }
    }

    public void ReturnToReal(){
        isReal = true;
        CheckCurrentWorld();
    }
}
